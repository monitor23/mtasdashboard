package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.mtasHome;
import ObjectRepositories.mtasLogin;

public class MTASDashboard2 {
	WebDriver driver;

	@BeforeTest
	public void Initializtion() {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://app.mtas.io/login");

	}

	@Test
	public void MTASLogin() throws InterruptedException {
		mtasLogin mLogin = new mtasLogin(driver);
		mLogin.Username().sendKeys("manu@mtas.io");
		mLogin.Password().sendKeys("Manu#1234");
		mLogin.Login().click();
		mtasHome mHome = new mtasHome(driver);
		mHome.DeviceStatus("+32474066817");

	}

}
