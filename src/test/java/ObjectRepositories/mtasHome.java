package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class mtasHome {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public mtasHome(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 30);
		PageFactory.initElements(driver, this);
	}

	public void DeviceStatus(String value) throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("tr.ant-table-row")));
		int rows = driver.findElements(By.cssSelector("tr.ant-table-row")).size();

		for (int i = 1; i <= rows; i++) {
			String msisdn = driver
					.findElement(By.cssSelector("tr.ant-table-row:nth-child(" + i + ") > td:nth-child(3)")).getText();

			if (msisdn.equals(value)) {
				System.out.println("MSISDN : " + msisdn);
				String status = driver
						.findElement(By.cssSelector("tr.ant-table-row:nth-child(" + i + ") > td:nth-child(1)"))
						.getText();
				if (status.equals("ONLINE")) {
					System.out.println("Device is ONLINE");
					this.CreateTestcase(value);
					this.ExecuteTestcase();
					this.DeleteTestcase();
					this.Logout();
					break;
				} else if (status.equals("OFFLINE")) {
					System.out.println("Device is OFFLINE");
					this.Logout();
					break;
				}

			} else {
				System.out.println("Device not Available in the list");
				this.Logout();
				break;
			}

		}
	}

	public void CreateTestcase(String msisdn) throws InterruptedException {
		Actions a = new Actions(driver);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("/html/body/div/section/section/aside/div[1]/ul/li[2]/ul/li[1]/a")));
		a.moveToElement(driver.findElement(By.xpath("/html/body/div/section/section/aside/div[1]/ul/li[2]/ul/li[1]/a")))
				.build().perform();
		driver.findElement(By.xpath("/html/body/div/section/section/aside/div[1]/ul/li[2]/ul/li[1]/a")).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".ant-btn")));
		driver.findElement(By.cssSelector(".ant-btn")).click();
		driver.findElement(By.id("add_test_case_form_name")).sendKeys("AutomationTesting-Youtube");
		driver.findElement(By.id("add_test_case_form_description")).sendKeys("By Albin");
		driver.findElement(By.id("add_test_case_form_medium")).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".ant-select-dropdown-menu")));
		driver.findElement(By.cssSelector("li.ant-select-dropdown-menu-item:nth-child(2)")).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("add_test_case_form_testData.url")));
		driver.findElement(By.id("add_test_case_form_testData.url"))
				.sendKeys("https://www.youtube.com/embed/4VSx2E7WE50");
		driver.findElement(By.id("add_test_case_form_testData.videoQuality")).click();
		Thread.sleep(1000);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".ant-select-dropdown-menu-item-active")));
		driver.findElement(By.cssSelector("li.ant-select-dropdown-menu-item-active:nth-child(1)")).click();
		Thread.sleep(1000);
		driver.findElement(By.id("add_test_case_form_testData.duration")).click();
		Thread.sleep(1000);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".ant-select-dropdown-menu-item-active")));
		driver.findElement(By.cssSelector("li.ant-select-dropdown-menu-item-active:nth-child(1)")).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(
				"div.ant-row:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > span:nth-child(1) > div:nth-child(1)")))
				.click();
		String device = driver.findElement(By.cssSelector(".ant-select-dropdown-menu-item-active")).getText();
		System.out.println(device);
		if (device.contains(msisdn)) {
			driver.findElement(By.cssSelector(".ant-select-dropdown-menu-item-active")).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("button.ant-btn:nth-child(1)")))
					.click();

		} else {
			System.out.println("Mobile Devices are OFFLINE.");
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.cssSelector("div.BtnRightAlign > button:nth-child(2)"))).click();
		}

	}

	public void ExecuteTestcase() throws InterruptedException {

		Thread.sleep(3000);
		Actions a = new Actions(driver);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("/html/body/div/section/section/aside/div[1]/ul/li[2]/ul/li[1]/a")));
		a.moveToElement(driver.findElement(By.xpath("/html/body/div/section/section/aside/div[1]/ul/li[2]/ul/li[1]/a")))
				.build().perform();
		driver.findElement(By.xpath("/html/body/div/section/section/aside/div[1]/ul/li[2]/ul/li[1]/a")).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.cssSelector("tr.ant-table-row:nth-child(1) > td:nth-child(2)")));
		String value = driver.findElement(By.cssSelector("tr.ant-table-row:nth-child(1) > td:nth-child(2)")).getText();
		if (value.contains("By Albin")) {
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
					By.cssSelector("tr.ant-table-row:nth-child(1) > td:nth-child(5)")));
			a.moveToElement(driver
					.findElement(By.cssSelector("tr.ant-table-row:nth-child(1) > td:nth-child(5) > i:nth-child(1)")))
					.build().perform();
			Thread.sleep(1000);
			driver.findElement(By.cssSelector("tr.ant-table-row:nth-child(1) > td:nth-child(5) > i:nth-child(1)"))
					.click();
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.cssSelector("li.ant-dropdown-menu-item:nth-child(1)"))).click();

			Thread.sleep(2000);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(
					".ant-tabs-tabpane > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(4) > h3:nth-child(1) > span:nth-child(2)")));
			String result = driver.findElement(By.cssSelector(
					".ant-tabs-tabpane > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(4) > h3:nth-child(1) > span:nth-child(2)"))
					.getText();

			if (result.equals("PASS")) {
				System.out.println("Test Result of " + value + " : " + result);
			} else if (result.equals("FAIL")) {
				System.out.println("Test Result of " + value + " : " + result);
			} else {
				System.out.println("Test Result not Available");
			}
		} else {
			System.out.println("Testcase Name not Available");
		}

	}

	public void DeleteTestcase() throws InterruptedException {
		Thread.sleep(3000);
		Actions a = new Actions(driver);
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("/html/body/div/section/section/aside/div[1]/ul/li[2]/ul/li[1]/a")));
		a.moveToElement(driver.findElement(By.xpath("/html/body/div/section/section/aside/div[1]/ul/li[2]/ul/li[1]/a")))
				.build().perform();
		driver.findElement(By.xpath("/html/body/div/section/section/aside/div[1]/ul/li[2]/ul/li[1]/a")).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions
				.visibilityOfAllElementsLocatedBy(By.cssSelector("tr.ant-table-row:nth-child(1) > td:nth-child(2)")));
		String value = driver.findElement(By.cssSelector("tr.ant-table-row:nth-child(1) > td:nth-child(2)")).getText();
		if (value.contains("By Albin")) {
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
					By.cssSelector("tr.ant-table-row:nth-child(1) > td:nth-child(5) > i:nth-child(1)")));
			driver.findElement(By.cssSelector("tr.ant-table-row:nth-child(1) > td:nth-child(5) > i:nth-child(1)"))
					.click();
			a.moveToElement(driver.findElement(By.cssSelector("li.ant-dropdown-menu-item:nth-child(4)"))).build()
					.perform();
			Thread.sleep(1000);
			driver.findElement(By.cssSelector("li.ant-dropdown-menu-item:nth-child(4)")).click();
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.cssSelector(".ant-modal-confirm-btns > button:nth-child(2)")))
					.click();

		}
	}

	public void Logout() throws InterruptedException {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("li.ant-menu-item:nth-child(4)")));
		Thread.sleep(3000);
		Actions a = new Actions(driver);
		a.moveToElement(driver.findElement(By.cssSelector("li.ant-menu-item:nth-child(4)"))).build().perform();
		Thread.sleep(2000);
		driver.findElement(By.cssSelector("li.ant-menu-item:nth-child(4)")).click();
		driver.close();
	}
}
